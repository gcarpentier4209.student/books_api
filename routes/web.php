<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return "Book API";
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('books',  ['uses' => 'BookController@showAllBooks']);

    $router->get('books/{id}', ['uses' => 'BookController@showOneBook']);

    $router->post('books', ['uses' => 'BookController@create']);

    $router->delete('books/{id}', ['uses' => 'BookController@delete']);

    $router->put('books/{id}', ['uses' => 'BookController@update']);

    $router->get('books/{id}/add_shelf', ['uses' => 'BookController@add_shelf']);

});
